<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request) {
        return $request->user();
    }

    public function userList(Request $request) {
        $users = User::where('id', '!=', $request->user()->id)->get();
        $friends = $request->user()->friends()->pluck("friend_id");

        return response()->json([
            "users" => $users,
            "friends" => $friends
        ]);
    }

    public function updateList(Request $request) {
        return $request->user()->friends()->sync($request->data);
    }
}
