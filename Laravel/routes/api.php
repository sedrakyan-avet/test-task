<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::get('user', [\App\Http\Controllers\UserController::class, 'index']);
    Route::post('user/list', [\App\Http\Controllers\UserController::class, 'userList']);
    Route::post('update/list', [\App\Http\Controllers\UserController::class, 'updateList']);
});
