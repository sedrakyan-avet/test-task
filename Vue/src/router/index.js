import { createRouter, createWebHistory } from 'vue-router'
import store from '../store'

/* Guest Component */
const LoginComponent = () =>
    import ('../components/Login.vue' /* webpackChunkName: "resource/js/components/login" */ )
const RegisterComponent = () =>
    import ('../components/Register.vue' /* webpackChunkName: "resource/js/components/register" */ )
    /* Guest Component */

/* Layouts */
const DashboardLayout = () =>
    import ('../components/layouts/Dashboard.vue')
    /* Layouts */

/* Authenticated Component */
const DashboardComponent = () =>
    import ('../components/Dashboard.vue' /* webpackChunkName: "resource/js/components/dashboard" */ )
    /* Authenticated Component */

const routes = [{
        name: "login",
        path: "/login",
        component: LoginComponent,
        meta: {
            middleware: "guest",
            title: 'Login'
        }
    },
    {
        name: "register",
        path: "/register",
        component: RegisterComponent,
        meta: {
            middleware: "guest",
            title: 'Register'
        }
    },
    {
        path: "/",
        component: DashboardLayout,
        meta: {
            middleware: "auth"
        },
        children: [{
            name: "dashboard",
            path: '/',
            component: DashboardComponent,
            meta: {
                title: 'Dashboard'
            }
        }]
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    // base: process.env.BASE_URL,
    routes
})


router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} - ${process.env.MIX_APP_NAME}`
    if (to.meta.middleware == "guest") {
        if (store.state.auth && store.state.auth.authenticated) {
            next({ name: "dashboard" })
        }
        next()
    } else {
        if (store.state.auth && store.state.auth.authenticated) {
            next()
        } else {
            next({ name: "login" })
        }
    }
})

export default router